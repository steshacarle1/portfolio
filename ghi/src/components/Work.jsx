import React from 'react';
import Studio16 from '../assets/studio16.png';
import BeepBeep from '../assets/beepbeep.png';
import PassportPals from '../assets/passportpals.png';
import ConferenceGo from '../assets/conferencego.png'

const Work = () => {
  return (
    <div name='work' className='w-full md:h-screen text-gray-300 bg-[#0a192f]'>
      <div className='max-w-[1000px] mx-auto p-4 flex flex-col justify-center w-full h-full'>
        <div className='pb-8'>
          <p className='text-4xl font-bold inline border-b-4 text-gray-300 border-[#40e0d0]'>Work</p>
          <p className='py-6'>Check out some of my recent work</p>
        </div>

        {/* container */}
        <div className='grid grid-cols-2 gap-4'>

          {/* grid item */}
          <div style={{ backgroundImage: `url(${PassportPals})`, backgroundPosition: 'top right' }} className='shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div'>

            {/* hover effects */}
            <div className='opacity-0 group-hover:opacity-100'>
              <span className='text-2xl font-bold text-white tracking-wider'>
                FastAPI React Application
              </span>
              <div className='pt-8 text-center'>
                <a href="https://passportpals.gitlab.io/module3-project-gamma/events/list" target="_blank" rel="noreferrer">
                <button className='text-center rounded-lg px-4 py-3 m-2 bg-white text-gray-700 font-bold text-lg'>Link</button>
                </a>
                <a href="https://gitlab.com/passportpals/module3-project-gamma" target="_blank" rel="noreferrer">
                <button className='text-center rounded-lg px-4 py-3 m-2 bg-white text-gray-700 font-bold text-lg'>Code</button>
                </a>
              </div>
            </div>
          </div>

          {/* grid item */}
          <div style={{ backgroundImage: `url(${BeepBeep})`, backgroundPosition: 'top right' }} className='shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div'>

            {/* hover effects */}
            <div className='opacity-0 group-hover:opacity-100'>
              <span className='text-2xl font-bold text-white tracking-wider'>
                Django React Application
              </span>
              <div className='pt-8 text-center'>
                {/* <a href="/">
                <button className='text-center rounded-lg px-4 py-3 m-2 bg-white text-gray-700 font-bold text-lg'>Demo</button>
                </a> */}
                <a href="https://gitlab.com/SteshaCarle/project-beta" target="_blank" rel="noreferrer">
                <button className='text-center rounded-lg px-4 py-3 m-2 bg-white text-gray-700 font-bold text-lg'>Code</button>
                </a>
              </div>
            </div>
          </div>
        </div>

        <div className='grid grid-cols-2 gap-4 mt-4'>

          {/* grid item */}
          <div style={{ backgroundImage: `url(${Studio16})`, backgroundPosition: 'top right' }} className='shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div'>

            {/* hover effects */}
            <div className='opacity-0 group-hover:opacity-100'>
              <span className='text-2xl font-bold text-white tracking-wider'>
                Django Application
              </span>
              <div className='pt-8 text-center'>
                {/* <a href="/">
                <button className='text-center rounded-lg px-4 py-3 m-2 bg-white text-gray-700 font-bold text-lg'>Demo</button>
                </a> */}
                <a href="https://gitlab.com/team-163974311/studio-16-task-manager" target="_blank" rel="noreferrer">
                <button className='text-center rounded-lg px-4 py-3 m-2 bg-white text-gray-700 font-bold text-lg'>Code</button>
                </a>
              </div>
            </div>
          </div>

          {/* grid item */}
          <div style={{ backgroundImage: `url(${ConferenceGo})`, backgroundPosition: 'top right' }} className='shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div'>

            {/* hover effects */}
            <div className='opacity-0 group-hover:opacity-100'>
              <span className='text-2xl font-bold text-white tracking-wider'>
                Django React Application
              </span>
              <div className='pt-8 text-center'>
                {/* <a href="/">
                <button className='text-center rounded-lg px-4 py-3 m-2 bg-white text-gray-700 font-bold text-lg'>Demo</button>
                </a> */}
                <a href="https://gitlab.com/SteshaCarle/fearless-frontend" target="_blank" rel="noreferrer">
                <button className='text-center rounded-lg px-4 py-3 m-2 bg-white text-gray-700 font-bold text-lg'>Code</button>
                </a>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  );
}

export default Work;
