import React from 'react'
import Stesha from '../assets/steshateti.png'

const About = () => {
  return (
    <div name='about' className='w-full h-screen bg-[#0a192f] text-gray-300'>
        <div className='flex flex-col justify-center items-center w-full h-full'>
            <div className='max-w-[1000px] w-full grid grid-cols-2 gap-8'>
                <div className='sm:text-right pb-8 pl-4'>
                    <p className='text-4xl font-bold inline border-b-4 border-[#40e0d0]'>About</p>
                </div>
                <div></div>
                </div>
            <div className='max-w-[1000px] w-full grid sm:grid-cols-2 gap-8 px-4 pb-16'>
                <div className='sm:text-right text-4xl font-bold'>
                    <p>Hi. I'm Stesha and this is Teti! It's nice to meet you.  Take a look around.</p>
                    <img src={Stesha} alt="Stesha" className='mt-5 p-2 border-2 border-[#40e0d0]'/>
                </div>
            <div>
                <p className='text-xl'>As a software engineer based in San Diego, CA, I am dedicated to crafting solutions
                    and continuously expanding my knowledge. With a deep love for problem-solving
                    and a knack for debugging, I find fulfillment in creating scalable code. Beyond coding, you'll find me enjoying the beautiful beaches of San Diego, playing beach volleyball, going for runs,
                    and nurturing my collection of house plants.</p>
            </div>
            {/* <img src={Stesha} alt="Stesha" className='p-2 border-2 border-[#40e0d0]'/> */}
            </div>
        </div>
    </div>
  )
}

export default About
