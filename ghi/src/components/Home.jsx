import React from 'react'
import {HiArrowNarrowRight} from 'react-icons/hi'
import { Link as ScrollLink } from 'react-scroll'

const Home = () => {

  // const viewWork = () => {
  //   const workSection = document.getElementById('work');
  //   if (workSection) {
  //     workSection.scrollIntoView({ behavior: 'smooth' });
  //   }
  // }

  return (
    <div name='home' className='w-full h-screen bg-[#0a192f]'>

        {/* container */}
        <div className='max-w-[1000px] mx-auto px-8 flex flex-col justify-center h-full'>
            {/* <p className='text-bold text-[#008080]'>Hi, my name is</p> */}
            <h1 className='text-4xl sm:text-7xl font-bold text-[#40e0d0] p-6'>Stesha Carlé</h1>
            <h2 className='text4xl sm:text-5xl font-bold text-[#8892b0] px-6'>Software Engineer</h2>
            <div className='p-6'>
                {/* <button className='text-white group border-2 px-6 py-3 my-2 flex items-center hover:bg-[#40e0d0] hover:border-[#40e0d0]'>View Work
                <span className='group-hover:rotate-90 duration 300'>
                <HiArrowNarrowRight className='ml-3 '/>
                </span>
                </button> */}
                <ScrollLink
                  to='work'
                  spy={true}
                  smooth={true}
                  offset={-70} // Adjust the offset as needed
                  duration={500}
                  style={{ padding: '8px 16px', maxWidth: '150px' }}
                  className='text-white group border-2 px-4 py-2 my-2 flex items-center hover:bg-[#40e0d0] hover:border-[#40e0d0]'
                >
                  View Work
                  <span className='group-hover:rotate-90 duration-300'>
                    <HiArrowNarrowRight className='ml-3' />
                  </span>
                </ScrollLink>
            </div>
        </div>
    </div>
  )
}

export default Home
